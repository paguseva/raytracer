#pragma once

#include <array>
#include <cmath>
#include <vec3.h>
#include <vector>

struct CameraOptions {
    int screen_width;
    int screen_height;
    double fov;
    std::array<double, 3> look_from;
    std::array<double, 3> look_to;

    CameraOptions(int width, int height, double fov = M_PI / 2,
                  std::array<double, 3> look_from = {0.0, 0.0, 0.0},
                  std::array<double, 3> look_to = {0.0, 0.0, -1.0})
        : screen_width(width),
          screen_height(height),
          fov(fov),
          look_from(std::move(look_from)),
          look_to(std::move(look_to))

    {
    }

    std::vector<Vec3<double>> CameraToWorldMatrix() const {
        Vec3<double> from(look_from);
        Vec3<double> to(look_to);

        Vec3 forward = from - to;
        forward.Normalize();

        Vec3<double> right(0);
        if (forward == Vec3(0.0, 1.0, 0.0)) {
            right = Vec3(1.0, 0.0, 0.0);
        } else if (forward == Vec3(0.0, -1.0, 0.0)) {
            right = Vec3(1.0, 0.0, 0.0);
        } else {
            right = Vec3(0.0, 1.0, 0.0).Cross(forward);
        }
        right.Normalize();

        Vec3 up = forward.Cross(right);
        up.Normalize();

        return {right, up, forward, from};
    }
};