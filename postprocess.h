#pragma once

#include <image.h>
#include <vec3.h>
#include <vector>

void ToneMapping(std::vector<std::vector<Vec3<double>>>* im) {
    size_t height = im->size(), width = (*im)[0].size();
    double max_value = (*im)[0][0].X();
    int nonzero = 0;
    for (size_t x = 0; x < width; ++x) {
        for (size_t y = 0; y < height; ++y) {
            Vec3<double> color = (*im)[y][x];
            if (color != Vec3<double>(0)) {
                ++nonzero;
            }
            if (color.X() > max_value) {
                max_value = color.X();
            }
            if (color.Y() > max_value) {
                max_value = color.Y();
            }
            if (color.Z() > max_value) {
                max_value = color.Z();
            }
        }
    }

    for (size_t x = 0; x < width; ++x) {
        for (size_t y = 0; y < height; ++y) {
            Vec3<double> color_vec = (*im)[y][x];
            color_vec = color_vec * (1.0 + color_vec / (max_value * max_value)) / (1.0 + color_vec);
            (*im)[y][x] = color_vec;
        }
    }
}

void GammaCorrection(std::vector<std::vector<Vec3<double>>>* im) {
    double power = 1.0 / 2.2;
    for (size_t y = 0; y < im->size(); ++y) {
        for (size_t x = 0; x < (*im)[0].size(); ++x) {
            (*im)[y][x] = (*im)[y][x] ^ power;
        }
    }
}