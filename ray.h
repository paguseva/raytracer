#pragma once

#include <camera_options.h>
#include <vec3.h>

class Ray {
public:
    Ray() : orig_(Vec3(0.0)), dir_(Vec3(0.0, 0.0, 1.0)) {
    }
    Ray(const Vec3<double>& orig, const Vec3<double>& dir) : orig_(orig), dir_(dir) {
        dir_.Normalize();
    }

    Ray Transform(const std::vector<Vec3<double>>& matrix) const {
        Vec3<double> new_orig = orig_.Transform(matrix) + matrix[3];
        double w = matrix[3].Dot(orig_) + 1;
        new_orig /= w;
        Vec3<double> new_dir = dir_.Transform(matrix);
        return {new_orig, new_dir};
    }

    Vec3<double> Origin() const {
        return orig_;
    }
    Vec3<double> Direction() const {
        return dir_;
    }

private:
    Vec3<double> orig_;
    Vec3<double> dir_;
};

std::optional<Ray> Refract(const Ray& original_ray, const Vec3<double>& normal,
                           const Vec3<double>& point, double refraction_index) {
    double inc_cos = -original_ray.Direction().Dot(normal);
    double eta = 1 / refraction_index;
    double k = 1 - eta * eta * (1 - inc_cos * inc_cos);
    if (k < 0) {
        // total internal reflection
        return {};
    }
    return Ray{point - normal * 1e-5,
               eta * original_ray.Direction() + (eta * inc_cos - sqrt(k)) * normal};
}

Ray Reflect(const Ray& original_ray, const Vec3<double>& normal, const Vec3<double>& point) {
    return Ray{point - normal * 1e-4,
               original_ray.Direction() - 2 * normal.Dot(original_ray.Direction()) * normal};
}
Ray CastCameraRay(int image_x, int image_y, const CameraOptions& camera_options) {
    int screen_width = camera_options.screen_width;
    int screen_height = camera_options.screen_height;
    double aspect_ratio = static_cast<double>(screen_width) / screen_height;
    double fov_tan = tan(camera_options.fov / 2.0);
    return {Vec3<double>(0),
            Vec3<double>(((image_x + 0.5) / screen_width * 2 - 1) * aspect_ratio * fov_tan,
                         (1 - (image_y + 0.5) / screen_height * 2) * fov_tan, -1)};
}

Ray CastRay(int image_x, int image_y, const CameraOptions& camera_options) {
    Ray camera_ray = CastCameraRay(image_x, image_y, camera_options);
    return camera_ray.Transform(camera_options.CameraToWorldMatrix());
}
